<?php

if (!defined('_PS_VERSION_')) {
  exit;
}



class FacebookLogin extends Module
{
  public function __construct()
  {
    $this->name = 'facebooklogin';
    $this->tab = 'front_office_features';
    $this->version = '1.0.0';
    $this->author = 'Mateusz Prasał';
    $this->need_instance = 0;
    $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    $this->bootstrap = true;

    parent::__construct();

    $this->displayName = $this->l('Facebook Login');
    $this->description = $this->l('Login to shop by Facebook Account');
    $this->controllers = array('login');
  }

  public function install()
  {
    if (!parent::install() || !$this->registerHook('displayNav') || !$this->registerHook('Header')) {
      return false;
    }
    $this->createDB();
    return true;
  }

  public function getContent()
  {
    return $this->_postProcess() . $this->_displayForm() . $this->_helperList();
  }

  public function _displayForm()
  {
    $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

    $helper = new HelperForm();

    // Module, token and currentIndex
    $helper->module = $this;
    $helper->name_controller = $this->name;
    $helper->token = Tools::getAdminTokenLite('AdminModules');
    $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

    // Language
    $helper->default_form_language = $default_lang;
    $helper->allow_employee_form_lang = $default_lang;

    // Title and toolbar
    $helper->title = $this->displayName;
    $helper->show_toolbar = true;        // false -> remove toolbar
    $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
    $helper->submit_action = 'submit'.$this->name;
    $helper->toolbar_btn = array(
        'save' =>
        array(
            'desc' => $this->l('Save'),
            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
            '&token='.Tools::getAdminTokenLite('AdminModules'),
        ),
        'back' => array(
            'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Back to list')
        )
    );

    // Load current value
    $helper->fields_value[$this->name . '_appId'] = Configuration::get($this->name . '_appId');

    return $helper->generateForm($this->_renderForm());
  }

  public function _renderForm()
  {
    $fields_form[0]['form'] = array(
        'legend' => array(
            'title' => $this->l('Ustawienia'),
        ),
        'input' => array(
            array(
                'type' => 'text',
                'label' => $this->l('App ID'),
                'name' => $this->name. '_appId',

                'required' => true
            )
        ),
        'submit' => array(
            'title' => $this->l('Save'),
            'class' => 'btn btn-default pull-right'
        )
    );

    return $fields_form;
  }

  public function _postProcess()
  {

    if (Tools::isSubmit('submit'.$this->name)) {
      Configuration::updateValue($this->name. '_appId',Tools::getValue($this->name. '_appId'));
      return $this->displayConfirmation($this->l('Wszystko w porządku'));
    }
  }

  public function _helperList()
  {

    $fields_list = array(
        'id_user' => array(
            'title' => $this->l('Id'),
            'width' => 140,
            'type' => 'text',
        ),
        'firstname' => array(
            'title' => $this->l('Firstname'),
            'width' => 140,
            'type' => 'text',
        ),
        'lastname' => array(
            'title' => $this->l('Lastname'),
            'width' => 140,
            'type' => 'text',
        ),
        'email' => array(
            'title' => $this->l('Email'),
            'width' => 140,
            'type' => 'text',
        ),
    );
    $helper = new HelperList();

    $helper->shopLinkType = '';

    $helper->simple_header = true;

    // Actions to be displayed in the "Actions" column
    // $helper->actions = array('view');

    $helper->identifier = 'id_user';
    $helper->show_toolbar = true;
    $helper->title = 'Użytkownicy zalogowani przez Facebook';
    $helper->table = 'facebookusers';
    $this->_helperlist = $helper;
    $users = Db::getInstance()->ExecuteS('SELECT * FROM '._DB_PREFIX_.'facebookusers');
    $helper->token = Tools::getAdminTokenLite('AdminModules');
    $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
    return $helper->generateList($users,$fields_list);

  }
  public function hookHeader($params)
  {
    Media::addJsDef(array('facebooklogin' => array(
      'loginToken' => Tools::encrypt('FacebookLoginToken'),
    )));
    $this->context->controller->addCSS(($this->_path).'views/css/facebooklogin.css');
    $this->context->controller->addJS($this->_path . 'views/js/front.js');

    $this->context->smarty->assign(['fbAppId'=>Configuration::get($this->name. '_appId')]);
    return $this->display(__FILE__,'header.tpl');

  }
  public function hookdisplayNav($params)
  {

    $this->context->controller->addJS($this->_path . 'views/js/front.js');

    return $this->display(__FILE__,'nav.tpl');
  }

  public function createDB()
  {
    $sql = "CREATE TABLE IF NOT EXISTS " . _DB_PREFIX_ . "facebookusers (
            id_user INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
            firstname varchar(255),
            lastname varchar(255),
            email varchar(255)

      ) CHARACTER SET utf8 COLLATE utf8_general_ci;";

      Db::getInstance()->execute($sql);
  }
}

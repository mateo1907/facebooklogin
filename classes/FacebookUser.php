<?php

class FacebookUser extends ObjectModel
{
  public $id_user;
  public $firstname;
  public $lastname;
  public $email;

  public static $definition = [
    'table' => 'facebookusers',
    'primary' => 'id_user',
    'fields' => [
      'id_user'           => ['type' => self::TYPE_INT, 'validate' => 'isInt'],
      'firstname'   => ['type'=>self::TYPE_STRING, 'db_type'=>'varchar(255)'],
      'lastname'   => ['type'=>self::TYPE_STRING, 'db_type'=>'varchar(255)'],
      'email'   => ['type'=>self::TYPE_STRING, 'db_type'=>'varchar(255)'],

    ]
  ];
}

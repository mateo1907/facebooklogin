<?php
require dirname(__FILE__).'/../../classes/FacebookUser.php';

class FacebookLoginLoginModuleFrontController extends ModuleFrontController
{


  public function postProcess()
  {
    if (Tools::getValue('token')) {
      if (Tools::encrypt('FacebookLoginToken') != Tools::getValue('token')) {
        die('Token is not valid');
      }
      $data = json_decode(Tools::getValue('data'));
      $link = $this->context->link->getPageLink('my-account',true);
      $customer = new Customer;
      $customer->getByEmail($data->email);

      if ($customer->id !== NULL) {
        // var_dump($data);
        $this->loginCustomer($customer);
        echo json_encode(['link'=>$link]);
        die();
      } else {
        $this->registerCustomer($customer,$data);
        echo json_encode(['link'=>$link]);
        die();
      }
    }
  }

  public function loginCustomer(Customer $customer)
  {
      /*Login when customer isset in customers table*/
      $context = Context::getContext();
      Hook::exec('actionBeforeAuthentication');

      if (_PS_VERSION_ <= 1.6) {
          if (isset($context->cookie->id_compare)) {
              $id_compare = $context->cookie->id_compare;
          } else {
              $id_compare = CompareProduct::getIdCompareByIdCustomer($customer->id);
          }
          $context->cookie->id_compare = $id_compare;
      }
      $context->cookie->id_customer = (int)($customer->id);
      $context->cookie->customer_lastname = $customer->lastname;
      $context->cookie->customer_firstname = $customer->firstname;
      $context->cookie->logged = 1;
      $customer->logged = 1;
      $context->cookie->is_guest = $customer->isGuest();
      $context->cookie->passwd = $customer->passwd;
      $context->cookie->email = $customer->email;
      /* Add customer to the context */
      $context->customer = $customer;
      if (Configuration::get('PS_CART_FOLLOWING')
          && (empty($context->cookie->id_cart)
          || Cart::getNbProducts($context->cookie->id_cart) == 0)
          && $id_cart = (int)Cart::lastNoneOrderedCart($context->customer->id)) {
          $context->cart = new Cart($id_cart);
      } else {
          $context->cart->id_carrier = 0;
          $context->cart->setDeliveryOption(null);
          $context->cart->id_address_delivery = (int)Address::getFirstCustomerAddressId((int)($customer->id));
          $context->cart->id_address_invoice = (int)Address::getFirstCustomerAddressId((int)($customer->id));
      }
      $context->cart->id_customer = (int)$customer->id;
      $context->cart->secure_key = $customer->secure_key;
      $context->cart->save();
      $context->cookie->id_cart = (int)$context->cart->id;
      $context->cookie->write();
      $context->cart->autosetProductAddress();
      Hook::exec('actionAuthentication');
      /* Login information have changed, so we check if the cart rules still apply */
      CartRule::autoRemoveFromCart($context);
      CartRule::autoAddToCart($context);
  }

  public function registerCustomer(Customer $customer, $data)
  {
    $user = new FacebookUser;
    $user->firstname = $data->first_name;
    $user->lastname = $data->last_name;
    $user->email = $data->email;
    $user->add();
    
    $customer->firstname = $data->first_name;
    $customer->lastname = $data->last_name;
    $customer->email = $data->email;
    $customer->passwd = md5(pSQL(_COOKIE_KEY_ . Tools::passwdGen()));
    $customer->is_guest = 0;
    $customer->active = 1;
    $customer->add();

    $this->loginCustomer($customer);
    return true;
  }


}

// Facebook login with JavaScript SDK
function fbLogin() {
    FB.login(function (response) {
        if (response.authResponse) {
            // Get and display the user profile data
            getFbUserData();
        } else {
            console.log('User cancelled login or did not fully authorize.')
        }
    }, {scope: 'email'});
}

// Fetch the user profile data from facebook
function getFbUserData(){
    FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
    function (response) {
        jQuery.ajax({
          url : baseUri + 'module/facebooklogin/login?token=' + facebooklogin.loginToken,
          type: 'post',
          data:{
            action : 'facebookLogin',
            data: JSON.stringify(response)
          }
        })
        .success(function(response){
          var json = $.parseJSON(response)
          window.location.href = json.link
        })


    });
}
